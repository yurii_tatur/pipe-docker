FROM alpine:3.8
RUN apk add --update --no-cache bash
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh
COPY . /
RUN chmod a+x /*.sh
ENTRYPOINT ["/entrypoint.sh"]