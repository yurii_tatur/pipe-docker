#!/usr/bin/env bash

# Mandatory globals
DOCKER_USERNAME=${DOCKER_USERNAME:? 'Dockerhub username is missing'}
DOCKER_PASSWORD=${DOCKER_PASSWORD:? 'Dockerhub password is missing'}
IMAGE_NAME=${IMAGE_NAME:? 'Docker image is missing'}
SSH_PRIVATE_KEY=${SSH_PRIVATE_KEY:? 'SSH private key is missing'}

# Optional globals
DOCKERFILE_NAME=${DOCKERFILE_NAME:= 'Dockerfile'}
DOCKER_REGISTRY_URL=${DOCKER_REGISTRY_URL:= 'https://hub.docker.com'}

source "/common.sh"

info 'Start building image'
docker build --build-arg "SSH_PRIVATE_KEY=${SSH_PRIVATE_KEY}" -f ${DOCKERFILE_NAME} -t ${IMAGE_NAME} .
info  'Docker registry login'
docker login --username ${DOCKER_USERNAME} --password ${DOCKER_PASSWORD}
info 'Pushing image to registry'
docker push ${IMAGE_NAME}
success 'Pipe finished'